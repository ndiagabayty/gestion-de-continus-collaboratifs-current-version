<?php 

 session_start();
 
 require("includes/init.php");
 require("filters/auth_filter.php");

if (!empty($_GET['id'])  &&  $_GET['id']!==get_session('id_user')) {
 	
 	$id = $_GET['id'];
 	// traitement
 	$query = $db->prepare("UPDATE friends_relationships
 	                       SET status='1' 
 	                       WHERE (id1_user = :id1_user AND id2_user= :id2_user)
                           OR (id1_user = :id2_user AND id2_user= :id1_user)");
 	$query->execute(
 		[ 
 		    'id1_user' =>get_session('id_user'),
 		    'id2_user' =>$id 
 		]);

// Sauvegarde de la notification
	 $query = $db->prepare('INSERT INTO notifications(id_subject, name, id_user)
	                        VALUES(:id_subject, :name, :id_user)');
	 $query ->execute(
	 	[
		 'id_subject' => $id,
		 'name' => 'friend_request_accepted',
		 'id_user' => get_session('id_user')
        ]);

 	set_flash("Vous êtes à présent ami avec cet utilisateur!");
 	redirection('profile.php?id='.$id);
 }else{
 	redirection('profile.php?id='.get_session('id_user'));
 }

?>
 