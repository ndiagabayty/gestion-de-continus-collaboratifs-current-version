

<?php $tilte='partage de codes sources';?>


<?php include("partials/_header.php"); ?>
  
  <div id="main-content">
     <div id="main-content-share-code">
       <form action="" autocomplete="off" method="post">
         <textarea name="code" id="code" required="required"
          placeholder="Entrer votre code ici"><?= echappe($code); ?></textarea>
         <div class="btn-group nav-code">
           <a href="share_code.php" class="btn btn-danger">Tout effacer</a>
           <input type="submit" name="save" value="Enregistrer" 
           class="btn btn-success">
         </div>
         
       </form>
     </div>
         
  </div>
    

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/tabby1.min.js"></script>
    <script>
      $('#code').tabby();
      $('#code').height( $(window).height() -50 );
    </script>

 
  </body>
</html>


     <?php include('partials/_footer.php'); ?>

 