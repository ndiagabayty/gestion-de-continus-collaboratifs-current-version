 
<?php $tilte='Edition de profile';?>


<?php include("partials/_header.php"); ?>
    
    
<div class="main-content">
    <div class="container">
        <div class="row">

 <?php if (!empty($_GET['id']) && $_GET['id']==get_session('id_user')): ?>
        <div class="col-md-6 col-md-offset-3">
            	    <div class="panel panel-default">
		                <div class="panel-heading">
		                	<h3 class="panel-title">Completer mon profil</h3>
		                </div>
		                <div class="panel-body">
		                	<?php include('partials/_error.php'); ?>

		                	<form data-parsley-validate method="post" 
		                	    autocomplete="off">
		                	  <div class="row">
		                	
		                	  	<div class="col-md-6">
		                	  		<div class="form-group">
		                	  			<label for="name">Nom<span class="text-danger">*</span></label>
		                	  			<input  type="text" name="name" id="name" 
		                	  			class="form-control"
		                	  			placeholder="john" 
		                	  			value="<?= recupere_infos_saisis('name') ?  : 
		                	  			    echappe($user->name) ?>" 
		                	  			required="required" 
		                	  			/>
		                	  		</div>
		                	  	</div>


                                <div class="col-md-6">
		                	  		<div class="form-group">
		                	  			<label for="city">Ville<span class="text-danger">*</span></label>
		                	  			<input  type="text" name="city" id="city" 
		                	  			class="form-control" 
		                	  			value="<?= recupere_infos_saisis('city') ?  : 
		                	  			    echappe($user->city) ?>" 
		                	  			required="required" 
		                	  			/>
		                	  		</div>
		                	  	</div>

		                	  	<div class="row">
		                	  		<div class="col-md-12">
		                	  			<div class="form-group">
		                	  			   <label for="avatar">Changer mon avatar</label>
		                	  			   <input type="file" name="avatar"
		                	  			    id="avatar">
		                	  			</div>
		                	  		</div>
		                	  	</div>


                                <div class="col-md-6">
		                	  		<div class="form-group">
		                	  			<label for="country">Pays<span class="text-danger">*</span></label>
		                	  			<input  type="text" name="country" id="country" 
		                	  			class="form-control"  
		                	  			value="<?= recupere_infos_saisis('country') ? : 
		                	  			    echappe($user->country) ?>" 
		                	  			required="required" 
		                	  			/>
		                	  		</div>
		                	  	</div>

		                	  	<div class="col-md-6">
		                	  		<div class="form-group">
		                	  			<label for="sexe">Sexe<span class="text-danger">*</span></label>
		                	  			<select name="sexe" id="sexe" 
		                	  			required="required" 
		                	  			class="form-control" >
		                	  			   <option value="H" <?= $user->sexe=="H" ? "selected" :""; ?>>
		                	  			   	Homme
		                	  			   </option>
		                	  			   <option value="F" <?= $user->sexe=="F" ? "selected" :""; ?>>
		                	  			   	Femme
		                	  			   </option>
		                	  			</select>
		                	  		</div>
		                	  	</div>

		                	  </div>

		                	  <div class="row">
		                	  	<div class="col-md-6">
		                	  		<div class="form-group">
		                	  			<label for="twitter">Twitter</label>
		                	  			<input  type="text" name="twitter" id="twitter" 
		                	  			class="form-control" 
		                	  			value="<?= recupere_infos_saisis('twitter') ? : 
		                	  			    echappe($user->twitter) ?>" />
		                	  			
		                	  		</div>
		                	  	</div>

		                	  	<div class="col-md-6">
		                	  		<div class="form-group">
		                	  			<label for="github">Github</label>
		                	  			<input  type="text" name="github" id="github" 
		                	  			class="form-control" 
		                	  			value="<?= recupere_infos_saisis('github') ? : 
		                	  			    echappe($user->github) ?>" />
		                	  			
		                	  		</div>
		                	  	</div>

		                	  </div>	


		                	  <div class="row">
		                	  	<div class="col-md-12">
		                	  		<div class="form-group">
		                	  			<label for="avaible_for_hiring">
		                	  				<input  type="checkbox" name="avaible_for_hiring" 
		                	  				id="avaible_for_hiring" <?= $user->avaible_for_hiring ? "checked" : "" ; ?> ></input>
		                	  			    Disponible pour emploi?
		                	  			</label>
		                	  			
		                	  			
		                	  		</div>
		                	  	</div>
		                	   </div>

		                	   <div class="row">
		                	  	<div class="col-md-12">
		                	  		<div class="form-group">
		                	  			<label for="bio">Biographie<span class="text-danger">*</span></label>
		                	  			<textarea  name="bio" id="bio" cols="25"
		                	  			 rows="10" required="required" 
		                	  			class="form-control" placeholder="je suis amoueux a la programmation"><?= echappe($user->bio) ?></textarea>  
		                	  			
		                	  		</div>
		                	  	</div>
		                	   </div>

		                	   <input type="submit" class="btn btn-primary" name="update" value="Valider">


		                	</form>

		                </div>
                    </div>  
            		
        </div>
            <?php endif; ?>
       </div>
    </div>
</div>
     <script src="assets/js/jquery.min.js"></script>
    <script src="librairies/uploadify/jquery.uploadify.min.js"></script>
    <script src="librairies/alertifyjs/alertify.js"></script>
    <script src="librairies/parsley/parsley.min.js"></script>
    <script src="librairies/parsley/i18n/fr.js"></script>
    <script type="text/javascript">
      window.ParsleyValidator.setLocale('fr');

    </script>
    <script type="text/javascript">
        <?php $timestamp = time() ;?>
    	$(function() {
      	$('#avatar').uploadify({
      		'fileObjName' : 'avatar',
      		'fileTypeDesc' : 'Images files',
      		'fileTypeExts' : '*.gif; *.jpg; *.jpeg; *.png',
      		'buttonText' :  'Parcourir',

      		'formData'    : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
					'id_user' : "<?= get_session('id_user') ?>",
					'<?php echo session_name(); ?>' : '<?php echo session_id(); ?>'
				},

      		'swf' :  'librairies/uploadify/uploadify.swf',
      		'uploader' :  'librairies/uploadify/uploadify.php' ,

      		'onUploadError' : function(file, errorCode, errorMsg, errorString){ 
      			alertify.error("Erreur lors de l'upload du fichier Veuillez reessayer SVP!");
      		},

      		'onUploadSuccess' : function(file,data,response){
      			//alertify.set('notifier','position','bottom-left');
      			alertify.success('Votre avatar  a ete bien uploade avec succes!');
      				//+alertify.get('notifier','position'));
      			window.location =  '/profile.php' ;
      		}
      	});
      });
    </script>
