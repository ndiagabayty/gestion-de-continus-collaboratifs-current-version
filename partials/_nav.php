

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="navbar" aria-expanded="true" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><?= WEBSITE_NAME?></a>
        </div>
        
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="list_user.php">Liste des utilisateurs</a></li>
            <li>
                <input type="search" name="" placeholder="Rechercher un utilisateur" id="searchbox" class="form-control">
                <div id="display-result">
                    
                    
                    
                </div>
            </li>
          </ul>
          <ul class="nav navbar-nav  navbar-right">
            <li class="<?= set_active('index') ?>"><a href="index.php">Accueil</a></li>

            <?php if (is_logged_in()): ?>

              <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">

                <img src="<?= get_session('avatar') ? get_session('avatar'):
                              get_avatar_url(get_session('email'),100) ?>" width ="20" height="20"
                              alt ="image de profil de <?=echappe(get_session('pseudo')) ?>"
                               class= "img-circle">

              <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li class="<?= set_active('profile') ?>">
                  <a href="profile.php?id=<?= get_session('id_user')?>">
                  <?= $menu['mon_profil'][$_SESSION['locale']]?></a>
                </li>
                <li class="<?= set_active('change_password') ?>">
                  <a href="change_password.php?id=<?= get_session('id_user')?>">
                  <?= $menu['change_password'][$_SESSION['locale']]?> </a>
                </li>
                <li class="<?= set_active('edit_user') ?>">
                  <a href="edit_user.php?id=<?= get_session('id_user')?>">
                  <?= $menu['edit_profil'][$_SESSION['locale']]?></a>
                </li>
                <li><a href="share_code.php"><?= $menu['share_code'][$_SESSION['locale']]?></a></li>
                <li role="separator" class="divider"></li>
                <li><a href="logout.php"><?= $menu['deconnexion'][$_SESSION['locale']]?></a></li>
              </ul>
            </li>
             <!---  Affichage des notifications !-->
            <li class="<?= $notifications_count > 0 ? 'have_notifs' : '' ?>">
               <a href="notifications.php"><i class="fa fa-bell"></i>
               <?= $notifications_count > 0 ? "($notifications_count)" : ''; ?>
               </a>
            </li>

            <?php else: ?>  
            <li class="<?= set_active('login') ?> "><a href="login.php">Connexion</a></li>
            <li class="<?= set_active('register') ?> "><a href="register.php">Inscription</a></li>
          <?php endif; ?>
          </ul>
        </div><!--/.nav-collapse -->
      </div> 
    </nav>