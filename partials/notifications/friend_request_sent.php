<a href="profile.php?id=<?= $notification->id_user?>">
	 <img src="<?= $notification->avatar ? $notification->avatar :
	  get_avatar_url($notification->email, 40) ?>" 
	  alt="Image de profil de <?= echappe($notification->pseudo) ?>" 
	  class="avatar-xs img-circle" width="50" height="50">
	 
	 <?= echappe($notification->pseudo) ?>
</a>
 vous a envoyé une demande d'amitié <span class="timeago" title="<?= $notification->created_at ?>">
 <?= $notification->created_at ?></span>.
 <a class="btn btn-primary" href="accept_friends.php?id=<?= $notification->id_user ?>">Accepter</a>
 
 <a class="btn btn-danger" href="delete_friends.php?id=<?= $notification->id_user ?>">Decliner</a>
 