


 <article class="media status-media" id="micropost<?=$micropost->m_id ?>">
   <div class="pull-left">
       <img src="<?= $micropost->avatar ? $micropost->avatar: get_avatar_url($micropost->email) ?>"
        alt="<?= $micropost->pseudo ?>" class="media-object avatar-xs img-circle"
         width="30" height="30">
       
   </div>
   <div class="media-body">
       <h4 class="media-heading"><?= echappe($micropost->pseudo); ?></h4>
       <p><i class="fa fa-clock-o"></i> <span class="timeago" title="<?= $micropost->created_at ?>">
       <?= $micropost->created_at ?></span>

       <?php if($micropost->id_user == get_session('id_user')): ?>
            <a data-confirm="Voulez-vous vraiment supprimer cette publication ?" 
               href="delete_micropost.php?id=<?= $micropost->m_id ?>"><i class="fa fa-trash"></i>
               Supprimer
            </a>
       <?php endif; ?> 
       
       </p>
       <?= nl2br(echappe($micropost->content)); ?>

       <hr>
            <?php if(user_has_already_like_the_micropost($micropost->m_id)): ?>
                <p><a href="unlike_micropost.php?id=<?=$micropost->m_id?>">Je n'aime plus</a></p>
            <?php else : ?>
                <p><a href="like_micropost.php?id=<?=$micropost->m_id?>">J'aime</a></p>
            <?php endif ;?>
       </hr>

       Nombre de j'aime(<?=$micropost->like_count ?>)
   </div>
 </article>