<?php 

 session_start();
 
 require("includes/init.php");
 require("filters/auth_filter.php");

if (!empty($_GET['id'])  &&  $_GET['id']!==get_session('id_user')) {
 	
 	$id = $_GET['id'];
 	// traitement
 	$query = $db->prepare("DELETE FROM friends_relationships
 	                       WHERE (id1_user = :id1_user AND id2_user= :id2_user)
                           OR (id1_user = :id2_user AND id2_user= :id1_user)");
 	$query->execute(
 		[ 
 		    'id1_user' =>get_session('id_user'),
 		    'id2_user' =>$id 
 		]);
 	set_flash("Vous etes plus ami avec cette utilisateur");
 	redirection('profile.php?id='.$id);
 }else{
 	redirection('profile.php?id='.get_session('id_user'));
 }

?>
 ?>